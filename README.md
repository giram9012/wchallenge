## WChallenge 
##### Juan Ramirez G

Start the app:
 
    * gradle bootrun
    
 There are three endpoint
 
 1- Registers a new licence (POST)
 
    * URL -> http://localhost:8080/wchallenge/licence
    * Body -> 
    {
        "albumId": 6,
        "userId": 5,
        "licenceType": "W"
    }
 
 2- Changes a licence (PUT)
  
     * URL -> http://localhost:8080/wchallenge/licence
     * Body -> 
     {
         "id": 1,
         "albumId": 6,
         "userId": 5,
         "licenceType": "W"
     }
       
3- Get all licences of some type ("R" | "W" | "RW") (get)
  
     * URL -> http://localhost:8080/wchallenge/licence?licenceType=R            