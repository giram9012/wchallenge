package com.wolox.wchallenge.licence;

import com.wolox.wchallenge.domain.licence.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public class LicenceRepositorySQL implements LicenceRepository {

    private final LicenceRepositorySpring repo;

    @Autowired
    public LicenceRepositorySQL(LicenceRepositorySpring repositorySpring) {
        this.repo = repositorySpring;
    }

    @Override
    public Mono<Licence> findById(Long id) {
        return repo.findById(id)
            .map(LicenceEntity::toDomain)
            .switchIfEmpty(Mono.just(Licence.builder().id(-1L).build()))
            .handle((licence1, sink) -> {
                    if (licence1.getId() == -1)
                        sink.error(new LicenceNotFoundException(id));
                    else sink.next(licence1);
                }
            );
    }

    @Override
    public Mono<Licence> record(Licence licence) {
        return repo.findLicence(licence.getAlbumId(), licence.getUserId())
            .map(LicenceEntity::toDomain)
            .map(licence1 -> {
                if (licence1.getType() != licence.getType())
                    throw new LicenceAlreadyExist(licence);
                return licence1;
            })
            .switchIfEmpty(
                repo.save(LicenceEntity.fromDomain(licence)).map(LicenceEntity::toDomain)
            );
    }

    @Override
    public Mono<Licence> change(Licence licence) {
        return repo.save(LicenceEntity.fromDomain(licence)).map(LicenceEntity::toDomain);
    }

    @Override
    public Flux<Licence> getAllBy(LicenceType licenceType) {
        return repo.findByLicenceType(licenceType.getType()).map(LicenceEntity::toDomain);
    }
}
