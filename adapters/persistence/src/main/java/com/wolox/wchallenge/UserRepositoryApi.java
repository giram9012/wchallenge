package com.wolox.wchallenge;

import com.wolox.wchallenge.domain.album.Album;
import com.wolox.wchallenge.domain.photo.Photo;
import com.wolox.wchallenge.domain.user.Company;
import com.wolox.wchallenge.domain.user.User;
import com.wolox.wchallenge.domain.user.UserRepository;
import com.wolox.wchallenge.domain.user.address.Address;
import com.wolox.wchallenge.domain.user.address.Geo;
import com.wolox.wchallenge.model.user.UserApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public class UserRepositoryApi implements UserRepository {

    private final UserApi userApi;

    @Autowired
    public UserRepositoryApi(UserApi userApi) {
        this.userApi = userApi;
    }

    private User[] fromUserModelToUserDomain(com.wolox.wchallenge.model.user.User[] users) {
        int usersSize = users.length;
        User[] usersDomain = new User[usersSize];
        for (int i = 0; i < usersSize; i++) {
            com.wolox.wchallenge.model.user.User user = users[i];
            Geo geo = new Geo(user.getAddressGeoLat(), user.getAddressGeoLng());
            Address address = new Address(user.getAddressStreet(), user.getAddressSuite(),
            user.getAddressCity(), user.getAddressZipcode(), geo);
            Company company = new Company(user.getName(), user.getCompanyCatchPhrase(), user.getCompanyBs());
            usersDomain[i] = new User(user.getId(), user.getName(), user.getUsername(), user.getEmail(),
                address, user.getPhone(), user.getWebsite(), company);
        }
        return usersDomain;
    }

    @Override
    public Mono<User[]> getAll() {
        return userApi.getAll().map(this::fromUserModelToUserDomain);
    }

    @Override
    public Mono<Album[]> albums(User user) {
        return null;
    }

    @Override
    public Mono<Photo[]> photos(User user) {
        return null;
    }
}
