package com.wolox.wchallenge;

import com.wolox.wchallenge.domain.album.Album;
import com.wolox.wchallenge.domain.album.AlbumRepository;
import com.wolox.wchallenge.domain.photo.Photo;
import com.wolox.wchallenge.model.album.AlbumApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public class AlbumRepositoryApi implements AlbumRepository {

    private final AlbumApi albumApi;

    @Autowired
    public AlbumRepositoryApi(AlbumApi albumApi) {
        this.albumApi = albumApi;
    }

    private Album[] fromAlbumModelToAlbumDomain(com.wolox.wchallenge.model.album.Album[] albums) {
        int albumSize = albums.length;
        Album[] albumsDomain = new Album[albumSize];
        for (int i = 0; i < albumSize; i++) {
            com.wolox.wchallenge.model.album.Album album = albums[i];
            albumsDomain[i] = new Album(album.getId(), album.getUserId(), album.getTitle());
        }
        return albumsDomain;
    }

    @Override
    public Mono<Album[]> getAll() {
        return albumApi.getAll().map(this::fromAlbumModelToAlbumDomain);
    }

    @Override
    public Mono<Photo[]> photos(Album album) {
        return null;
    }
}
