package com.wolox.wchallenge;

import com.wolox.wchallenge.domain.photo.Photo;
import com.wolox.wchallenge.domain.photo.PhotoRepository;
import com.wolox.wchallenge.model.photo.PhotoApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public class PhotoRepositoryApi implements PhotoRepository {

    private final PhotoApi photoApi;

    @Autowired
    public PhotoRepositoryApi(PhotoApi photoApi) {
        this.photoApi = photoApi;
    }

    private Photo[] fromPhotoModelToPhotoDomain(com.wolox.wchallenge.model.photo.Photo[] photos) {
        int photosSize = photos.length;
        Photo[] photosDomain = new Photo[photosSize];
        for (int i = 0; i < photosSize; i++) {
            com.wolox.wchallenge.model.photo.Photo photo = photos[i];
            photosDomain[i] = new Photo(photo.getId(), photo.getAlbumId(), photo.getTitle(),
                photo.getUrl(), photo.getThumbnailUrl());
        }
        return photosDomain;
    }

    @Override
    public Mono<Photo[]> getAll() {
        return photoApi.getAll().map(this::fromPhotoModelToPhotoDomain);
    }
}
