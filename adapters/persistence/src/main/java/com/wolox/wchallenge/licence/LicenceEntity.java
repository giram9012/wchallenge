package com.wolox.wchallenge.licence;

import com.wolox.wchallenge.domain.licence.Licence;
import com.wolox.wchallenge.domain.licence.LicenceType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
@ToString
public class LicenceEntity {

    @Id
    @Column("id")
    private Long id;
    @Column("album_id")
    private Long albumId;
    @Column("user_id")
    private Long userId;
    @Column("licence_type")
    private String licenceType;

    public static final LicenceEntity fromDomain(Licence licence) {
        LicenceEntity licenceE = new LicenceEntity();
        if (licence.getId() != null)
            licenceE.setId(licence.getId());
        licenceE.setAlbumId(licence.getAlbumId());
        licenceE.setUserId(licence.getUserId());
        licenceE.setLicenceType(licence.getType());
        return licenceE;
    }

    public static final Licence toDomain(LicenceEntity licenceE) {
        return Licence.builder()
            .id(licenceE.getId())
            .albumId(licenceE.getAlbumId())
            .userId((licenceE.getUserId()))
            .licenceType(LicenceType.valueOf(licenceE.getLicenceType()))
            .build();
    }
}
