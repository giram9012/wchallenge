package com.wolox.wchallenge.licence;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface LicenceRepositorySpring extends R2dbcRepository<LicenceEntity, Long> {

    @Query("SELECT * FROM licence_entity WHERE licence_type like $1")
    Flux<LicenceEntity> findByLicenceType(String licenceType);

    @Query("SELECT * FROM licence_entity WHERE album_id = $1 AND user_id = $2 LIMIT 1")
    Mono<LicenceEntity> findLicence(Long albumId, Long userId);
}
