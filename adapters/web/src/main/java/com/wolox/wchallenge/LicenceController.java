package com.wolox.wchallenge;

import com.wolox.wchallenge.application.port.in.licence.ChangeLicence;
import com.wolox.wchallenge.application.port.in.licence.LicenceQuery;
import com.wolox.wchallenge.application.port.in.licence.RecordLicence;
import com.wolox.wchallenge.domain.licence.Licence;
import com.wolox.wchallenge.domain.licence.LicenceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/wchallenge/licence")
public class LicenceController {

    private final RecordLicence recordLicence;
    private final ChangeLicence changeLicence;
    private final LicenceQuery licenceQuery;

    @Autowired
    public LicenceController(RecordLicence recordLicence, ChangeLicence changeLicence, LicenceQuery licenceQuery) {
        this.recordLicence = recordLicence;
        this.changeLicence = changeLicence;
        this.licenceQuery = licenceQuery;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Mono<Licence> record(@RequestBody RecordLicence.LicenceCommand command) {
        return recordLicence.record(command);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Mono<Licence> change(@RequestBody ChangeLicence.LicenceCommand command) {
        return changeLicence.change(command);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Flux<Licence> getAllByType(@RequestParam String licenceType) {
        return licenceQuery.getAllBy(LicenceType.valueOf(licenceType.toUpperCase()));
    }
}
