package com.wolox.wchallenge;

import com.wolox.wchallenge.domain.licence.LicenceAlreadyExist;
import com.wolox.wchallenge.domain.licence.LicenceDataInconsistent;
import com.wolox.wchallenge.domain.licence.LicenceNotFoundException;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@CommonsLog
public class ExceptionController {

    @ExceptionHandler(value = {LicenceNotFoundException.class})
    protected ResponseEntity<Object> notFoundException(LicenceNotFoundException ex) {
        log.warn(ex.getMessage());
        return buildResponseEntity(HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {LicenceAlreadyExist.class})
    protected ResponseEntity<Object> alreadyException(LicenceAlreadyExist ex) {
        log.warn(ex.getMessage());
        return buildResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {LicenceDataInconsistent.class})
    protected ResponseEntity<Object> inconsistentDataException(LicenceDataInconsistent ex) {
        log.warn(ex.getMessage());
        return buildResponseEntity(HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<Object> buildResponseEntity(HttpStatus httpStatus) {
        return new ResponseEntity<>(httpStatus);
    }
}
