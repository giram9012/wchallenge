package com.wolox.wchallenge.infrastructure.cache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.wolox.wchallenge.model.album.Album;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Service
public class AlbumCache {

    private Cache<String, Album[]> albumsCache = Caffeine.newBuilder().maximumSize(3)
        .expireAfterWrite(Duration.ofMinutes(5)).build();
    ;
    // private Cache<Long, Photo[]> photosByAlbumId;

    public Album[] getAlbums(String pathK) {
        return albumsCache.get(pathK, k -> new Album[0]);
    }

    public void putAlbums(String pathK, Album[] albums) {
        albumsCache.put(pathK, albums);
    }

}
