package com.wolox.wchallenge.infrastructure.api;

import org.springframework.web.reactive.function.client.WebClient;

public abstract class Client {

    private final static String URL_BASE = "https://jsonplaceholder.typicode.com";

    protected WebClient newWebClient() {
        return WebClient.builder().baseUrl(URL_BASE).build();
    }
}
