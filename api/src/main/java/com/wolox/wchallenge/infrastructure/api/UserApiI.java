package com.wolox.wchallenge.infrastructure.api;

import com.wolox.wchallenge.infrastructure.cache.UserCache;
import com.wolox.wchallenge.model.user.User;
import com.wolox.wchallenge.model.user.UserApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class UserApiI extends Client implements UserApi {

    private final static String PATH = "/users";
    private UserCache userCache;

    @Autowired
    public UserApiI(UserCache userCache) {
        this.userCache = userCache;
    }

    @Override
    public Mono<User[]> getAll() {
        User[] cachedUsers = userCache.getUsers(PATH);
        if (cachedUsers.length > 0)
            return Mono.just(cachedUsers);
        return newWebClient()
            .get()
            .uri(PATH)
            .retrieve()
            .bodyToMono(User[].class)
            .doOnNext(response -> userCache.putUsers(PATH, response));
    }
}