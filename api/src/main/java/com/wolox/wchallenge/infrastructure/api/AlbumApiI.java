package com.wolox.wchallenge.infrastructure.api;

import com.wolox.wchallenge.infrastructure.cache.AlbumCache;
import com.wolox.wchallenge.model.album.Album;
import com.wolox.wchallenge.model.album.AlbumApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class AlbumApiI extends Client implements AlbumApi {

    private final static String PATH = "/albums";
    private AlbumCache albumCache;

    @Autowired
    public AlbumApiI(AlbumCache albumCache) {
        this.albumCache = albumCache;
    }

    @Override
    public Mono<Album[]> getAll() {
        Album[] cachedAlbums = albumCache.getAlbums(PATH);
        if (cachedAlbums.length > 0)
            return Mono.just(cachedAlbums);
        return newWebClient()
            .get()
            .uri(PATH)
            .retrieve()
            .bodyToMono(Album[].class)
            .doOnNext(response -> albumCache.putAlbums(PATH, response));
    }
}
