package com.wolox.wchallenge.infrastructure.cache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.wolox.wchallenge.model.user.User;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
public class UserCache {
    private Cache<String, User[]> userCache = Caffeine.newBuilder().maximumSize(3)
        .expireAfterWrite(Duration.ofMinutes(5)).build();

    public User[] getUsers(String pathK) {
        return userCache.get(pathK, k -> new User[0]);
    }

    public void putUsers(String pathK, User[] users) {
        userCache.put(pathK, users);
    }
}
