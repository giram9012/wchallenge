package com.wolox.wchallenge.infrastructure.api;

import com.wolox.wchallenge.infrastructure.cache.PhotoCache;
import com.wolox.wchallenge.model.photo.Photo;
import com.wolox.wchallenge.model.photo.PhotoApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class PhotoApiI extends Client implements PhotoApi {

    private final static String PATH = "/photos";
    private PhotoCache photoCache;

    @Autowired
    public PhotoApiI(PhotoCache photoCache) {
        this.photoCache = photoCache;
    }

    @Override
    public Mono<Photo[]> getAll() {
        Photo[] cachedPhotos = photoCache.getPhotos(PATH);
        if (cachedPhotos.length > 0)
            return Mono.just(cachedPhotos);
        return newWebClient()
            .get()
            .uri(PATH)
            .retrieve()
            .bodyToMono(Photo[].class)
            .doOnNext(response -> photoCache.putPhotos(PATH, response));
    }
}
