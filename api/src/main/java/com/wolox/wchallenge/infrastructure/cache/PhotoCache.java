package com.wolox.wchallenge.infrastructure.cache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.wolox.wchallenge.model.photo.Photo;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
public class PhotoCache {
    private Cache<String, Photo[]> photoCache = Caffeine.newBuilder().maximumSize(3)
        .expireAfterWrite(Duration.ofMinutes(5)).build();

    public Photo[] getPhotos(String pathK) {
        return photoCache.get(pathK, k -> new Photo[0]);
    }

    public void putPhotos(String pathK, Photo[] photos) {
        photoCache.put(pathK, photos);
    }
}
