package com.wolox.wchallenge.model.photo;

import reactor.core.publisher.Mono;

public interface PhotoApi {
    Mono<Photo[]> getAll();
}
