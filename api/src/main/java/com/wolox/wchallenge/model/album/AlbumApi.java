package com.wolox.wchallenge.model.album;

import com.wolox.wchallenge.model.album.Album;
import reactor.core.publisher.Mono;

public interface AlbumApi {
    Mono<Album[]> getAll();
}
