package com.wolox.wchallenge.model.user;

import com.wolox.wchallenge.model.user.address.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Long id;
    private String name;
    private String username;
    private String email;
    private Address address;
    private String phone;
    private String website;
    private Company company;


    public String getAddressStreet() {
        return this.address.getStreet();
    }

    public String getAddressSuite() {
        return this.address.getSuite();
    }

    public String getAddressCity() {
        return this.address.getCity();
    }

    public String getAddressZipcode() {
        return this.address.getZipcode();
    }

    public String getAddressGeoLat() {
        return this.address.getGeoLat();
    }

    public String getAddressGeoLng() {
        return this.address.getGeoLng();
    }

    public String getCompanyName() {
        return this.company.getName();
    }

    public String getCompanyCatchPhrase() {
        return this.company.getCatchPhrase();
    }

    public String getCompanyBs() {
        return this.company.getBs();
    }
}
