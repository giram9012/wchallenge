package com.wolox.wchallenge.model.user;

import reactor.core.publisher.Mono;

public interface UserApi {
    Mono<User[]> getAll();
}
