package com.wolox.wchallenge.model.user.address;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    private String street;
    private String suite;
    private String city;
    private String zipcode;
    private Geo geo;

    public String getGeoLat() {
        return this.geo.getLat();
    }

    public String getGeoLng() {
        return this.geo.getLng();
    }
}
