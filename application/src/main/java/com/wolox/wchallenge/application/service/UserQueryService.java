package com.wolox.wchallenge.application.service;

import com.wolox.wchallenge.application.port.in.AlbumQuery;
import com.wolox.wchallenge.application.port.in.UserQuery;
import com.wolox.wchallenge.domain.album.Album;
import com.wolox.wchallenge.domain.photo.Photo;
import com.wolox.wchallenge.domain.user.User;
import com.wolox.wchallenge.domain.user.UserNotFoundException;
import com.wolox.wchallenge.domain.user.UserRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;

@Service
public class UserQueryService implements UserQuery {

    private final UserRepository userRepository;
    private final AlbumQuery albumQuery;

    public UserQueryService(UserRepository userRepository, AlbumQuery albumQuery) {
        this.userRepository = userRepository;
        this.albumQuery = albumQuery;
    }

    @Override
    public Mono<User[]> getAll() {
        return userRepository.getAll();
    }

    @Override
    public Mono<User> findById(User[] users, Long id) {
        return Mono.just(new ArrayList<>(Arrays.asList(users)).stream()
            .filter(user -> user.getId() == id)
            .findFirst() // TODO get from repo if no exist
            .orElseThrow(() -> new UserNotFoundException(id)));
    }

    @Override
    public Mono<Boolean> contains(User[] users, Long id) {
        return Mono.just(new ArrayList<>(Arrays.asList(users)).stream()
            .anyMatch(user -> user.getId() == id)); // TODO get from repo if no exist
    }

    @Override
    public Mono<Album[]> albums(User user) {
        return userRepository.albums(user);
    }

    @Override
    public Mono<Photo[]> photos(User user) {
        return userRepository.albums(user)
            .flatMapMany(albums ->
                Flux.fromArray(albums)
                    .flatMap(album -> albumQuery.photos(album))

            ).collectList()
            .map(photosByAlbum -> this.concatenatePhotos(photosByAlbum));
    }

    private Photo[] concatenatePhotos(List<Photo[]> photosByAlbum) {
        Photo[] photos = new Photo[0];
        return photosByAlbum.stream().reduce(photos, combinePhotos);
    }

    private BinaryOperator<Photo[]> combinePhotos =
        (ph1, ph2) -> {
            int ph1Len = ph1.length;
            int ph2Len = ph2.length;
            Photo[] result = new Photo[ph1Len + ph2Len];
            System.arraycopy(ph1, 0, result, 0, ph1Len);
            System.arraycopy(ph2, 0, result, ph1Len, ph2Len);
            return result;
        };
}
