package com.wolox.wchallenge.application.port.in;

import com.wolox.wchallenge.domain.album.Album;
import com.wolox.wchallenge.domain.photo.Photo;
import reactor.core.publisher.Mono;

public interface AlbumQuery {

    Mono<Album[]> getAll();

    Mono<Album> findById(Album[] albums, Long id);

    Mono<Boolean> contains(Album[] albums, Long id);

    Mono<Photo[]> photos(Album album);
}
