package com.wolox.wchallenge.application.port.in;

import com.wolox.wchallenge.domain.photo.Photo;
import reactor.core.publisher.Mono;

public interface PhotoQuery {

    Mono<Photo[]> getAll();
}
