package com.wolox.wchallenge.application.service.licence;

import com.wolox.wchallenge.application.port.in.licence.ChangeLicence;
import com.wolox.wchallenge.application.port.in.licence.LicenceQuery;
import com.wolox.wchallenge.domain.licence.*;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class ChangeLicenceService implements ChangeLicence {

    private final LicenceRepository licenceRepository;
    private final LicenceQuery licenceQuery;

    public ChangeLicenceService(LicenceRepository licenceRepository, LicenceQuery licenceQuery) {
        this.licenceRepository = licenceRepository;
        this.licenceQuery = licenceQuery;
    }

    @Override
    public Mono<Licence> change(LicenceCommand command) {
        return licenceQuery.findById(command.getId())
            .zipWith(licenceQuery.existRecordValues(command.toDomain()))
            .flatMap(result -> {
                Licence licence = result.getT1();
                if (!result.getT2())
                    throw new LicenceDataInconsistent(licence);
                else if (licence.getAlbumId() == command.getAlbumId()
                && licence.getUserId() == command.getUserId())
                    throw new LicenceAlreadyExist(licence);
                return licenceRepository.change(
                    Licence.builder()
                        .id(command.getId())
                        .albumId(command.getAlbumId())
                        .userId(command.getUserId())
                        .licenceType(LicenceType.valueOf(command.getLicenceType()))
                        .build());
            });
    }
}
