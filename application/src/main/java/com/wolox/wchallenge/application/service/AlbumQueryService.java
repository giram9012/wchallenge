package com.wolox.wchallenge.application.service;

import com.wolox.wchallenge.application.port.in.AlbumQuery;
import com.wolox.wchallenge.domain.album.Album;
import com.wolox.wchallenge.domain.album.AlbumNotFoundException;
import com.wolox.wchallenge.domain.album.AlbumRepository;
import com.wolox.wchallenge.domain.photo.Photo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Arrays;

@Service
public class AlbumQueryService implements AlbumQuery {

    private final AlbumRepository albumRepository;

    @Autowired
    public AlbumQueryService(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }

    public Mono<Album[]> getAll() {
        return albumRepository.getAll();
    }

    @Override
    public Mono<Album> findById(Album[] albums, Long id) {
        return Mono.just(new ArrayList<>(Arrays.asList(albums)).stream()
            .filter(album -> album.getId() == id)
            .findFirst()
            .orElseThrow(() -> new AlbumNotFoundException(id)));
    }

    @Override
    public Mono<Boolean> contains(Album[] albums, Long id) {
        return Mono.just(new ArrayList<>(Arrays.asList(albums)).stream()
            .anyMatch(album -> album.getId() == id));
    }

    @Override
    public Mono<Photo[]> photos(Album album) {
        return albumRepository.photos(album);
    }
}
