package com.wolox.wchallenge.application.port.in;

import com.wolox.wchallenge.domain.album.Album;
import com.wolox.wchallenge.domain.photo.Photo;
import com.wolox.wchallenge.domain.user.User;
import reactor.core.publisher.Mono;

public interface UserQuery {

    Mono<User[]> getAll();

    Mono<User> findById(User[] users, Long id);

    Mono<Boolean> contains(User[] users, Long id);

    Mono<Album[]> albums(User user);

    Mono<Photo[]> photos(User user);
}
