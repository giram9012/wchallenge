package com.wolox.wchallenge.application.port.in.licence;

import com.wolox.wchallenge.domain.licence.Licence;
import com.wolox.wchallenge.domain.licence.LicenceType;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface LicenceQuery {

    Flux<Licence> getAllBy(LicenceType licenceType);

    Mono<Licence> findById(Long id);

    Mono<Boolean> existRecordValues(Licence licence);
}
