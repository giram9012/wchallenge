package com.wolox.wchallenge.application.service.licence;

import com.wolox.wchallenge.application.port.in.licence.LicenceQuery;
import com.wolox.wchallenge.application.port.in.licence.RecordLicence;
import com.wolox.wchallenge.domain.licence.Licence;
import com.wolox.wchallenge.domain.licence.LicenceDataInconsistent;
import com.wolox.wchallenge.domain.licence.LicenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class RecordLicenceService implements RecordLicence {

    private final LicenceRepository licenceRepository;
    private final LicenceQuery licenceQuery;


    @Autowired
    public RecordLicenceService(LicenceRepository licenceRepository, LicenceQuery licenceQuery) {
        this.licenceRepository = licenceRepository;
        this.licenceQuery = licenceQuery;
    }

    @Override
    public Mono<Licence> record(LicenceCommand command) {
        return licenceQuery.existRecordValues(command.toDomain())
            .flatMap(exist -> {
                Licence licence = command.toDomain();
                if (exist)
                    return licenceRepository.record(licence);
                throw new LicenceDataInconsistent(licence);
            });
    }
}























