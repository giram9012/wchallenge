package com.wolox.wchallenge.application.port.in.licence;

import com.wolox.wchallenge.application.SelfValidating;
import com.wolox.wchallenge.domain.licence.Licence;
import com.wolox.wchallenge.domain.licence.LicenceType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Value;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public interface RecordLicence {

    Mono<Licence> record(LicenceCommand command);

    @Data
    @EqualsAndHashCode(callSuper = false)
    class LicenceCommand extends SelfValidating {

        @NotNull
        private Long albumId;
        @NotNull
        private Long userId;
        @Pattern(regexp = "R|W|RW")
        private String licenceType;

        public Licence toDomain() {
            return Licence.builder()
                .albumId(this.getAlbumId())
                .userId(this.getUserId())
                .licenceType(LicenceType.valueOf(this.getLicenceType()))
                .build();
        }
    }
}
