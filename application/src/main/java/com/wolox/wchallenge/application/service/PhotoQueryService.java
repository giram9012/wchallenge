package com.wolox.wchallenge.application.service;

import com.wolox.wchallenge.application.port.in.PhotoQuery;
import com.wolox.wchallenge.domain.photo.Photo;
import com.wolox.wchallenge.domain.photo.PhotoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class PhotoQueryService implements PhotoQuery {

    private final PhotoRepository photoRepository;

    public PhotoQueryService(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    @Override
    public Mono<Photo[]> getAll() {
        return photoRepository.getAll();
    }
}
