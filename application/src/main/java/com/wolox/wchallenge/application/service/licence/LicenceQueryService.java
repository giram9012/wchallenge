package com.wolox.wchallenge.application.service.licence;

import com.wolox.wchallenge.application.port.in.AlbumQuery;
import com.wolox.wchallenge.application.port.in.UserQuery;
import com.wolox.wchallenge.application.port.in.licence.LicenceQuery;
import com.wolox.wchallenge.domain.licence.Licence;
import com.wolox.wchallenge.domain.licence.LicenceRepository;
import com.wolox.wchallenge.domain.licence.LicenceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class LicenceQueryService implements LicenceQuery {

    private final LicenceRepository licenceRepository;
    private final AlbumQuery albumQuery;
    private final UserQuery userQuery;

    @Autowired
    public LicenceQueryService(LicenceRepository licenceRepository, AlbumQuery albumQuery, UserQuery userQuery) {
        this.licenceRepository = licenceRepository;
        this.albumQuery = albumQuery;
        this.userQuery = userQuery;
    }

    @Override
    public Flux<Licence> getAllBy(LicenceType licenceType) {
        return licenceRepository.getAllBy(licenceType);
    }

    @Override
    public Mono<Licence> findById(Long id) {
        return licenceRepository.findById(id);
    }

    @Override
    public Mono<Boolean> existRecordValues(Licence licence) {
        return albumQuery.getAll()
            .flatMap(albums -> albumQuery.contains(albums, licence.getAlbumId()))
            .zipWith(
                userQuery.getAll().flatMap(
                    users -> userQuery.contains(users, licence.getUserId())
                ))
            .flatMap(r -> Mono.just(r.getT1() && r.getT2()));
    }
}
