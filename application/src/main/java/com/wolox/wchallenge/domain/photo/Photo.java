package com.wolox.wchallenge.domain.photo;

import lombok.Value;

@Value
public class Photo {
    private Long id;
    private Long albumId;
    private String title;
    private String url;
    private String thumbnailUrl;
}
