package com.wolox.wchallenge.domain.licence;

import com.wolox.wchallenge.domain.DomainException;

public class LicenceDataInconsistent extends DomainException {

    private static final String MESSAGE = "The licence data is inconsistent verify it. %s";

    public LicenceDataInconsistent(Licence licence) {
        super(String.format(MESSAGE, licence.toString()));
    }
}
