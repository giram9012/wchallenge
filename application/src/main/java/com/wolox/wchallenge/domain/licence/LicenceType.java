package com.wolox.wchallenge.domain.licence;

public enum LicenceType {
    R("R"),W("W"),RW("RW");

    private String type;

    LicenceType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
