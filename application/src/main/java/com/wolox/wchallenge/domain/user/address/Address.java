package com.wolox.wchallenge.domain.user.address;

import lombok.Value;

@Value
public class Address {
    private String street;
    private String suite;
    private String city;
    private String zipcode;
    private Geo geo;

    public String getGeoLat() {
        return this.geo.getLat();
    }

    public String getGeoLng() {
        return this.geo.getLng();
    }
}
