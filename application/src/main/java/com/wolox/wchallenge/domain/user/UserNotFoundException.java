package com.wolox.wchallenge.domain.user;

import com.wolox.wchallenge.domain.DomainException;

public class UserNotFoundException extends DomainException {
    private static final String MESSAGE = "The user with id = %d not found";

    public UserNotFoundException(Long id) {
        super(String.format(MESSAGE, id));
    }
}
