package com.wolox.wchallenge.domain.user;

import com.wolox.wchallenge.domain.album.Album;
import com.wolox.wchallenge.domain.photo.Photo;
import reactor.core.publisher.Mono;

public interface UserRepository {

    Mono<User[]> getAll();
    Mono<Album[]> albums(User user);
    Mono<Photo[]> photos(User user);
}
