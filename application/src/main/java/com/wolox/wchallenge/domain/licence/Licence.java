package com.wolox.wchallenge.domain.licence;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;

@Value
@Builder
@ToString
public class Licence {

    private Long id;
    private Long albumId;
    private Long userId;
    private LicenceType licenceType;

    public String getType() {
        return this.licenceType.getType();
    }

}
