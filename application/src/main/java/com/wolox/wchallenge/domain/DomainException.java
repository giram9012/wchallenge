package com.wolox.wchallenge.domain;

public class DomainException extends RuntimeException {

    public DomainException(String message){
        super(message);
    }
}
