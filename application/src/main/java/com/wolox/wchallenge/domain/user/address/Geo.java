package com.wolox.wchallenge.domain.user.address;

import lombok.Value;

@Value
public class Geo {
    private String lat;
    private String lng;
}
