package com.wolox.wchallenge.domain.album;

import lombok.Value;

@Value
public class Album {
    private Long id;
    private Long userId;
    private String title;
}
