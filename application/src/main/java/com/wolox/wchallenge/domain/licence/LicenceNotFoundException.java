package com.wolox.wchallenge.domain.licence;

import com.wolox.wchallenge.domain.DomainException;

public class LicenceNotFoundException extends DomainException {

    private static final String MESSAGE = "The licence with id = %d not found";

    public LicenceNotFoundException(Long id) {
        super(String.format(MESSAGE, id));
    }
}
