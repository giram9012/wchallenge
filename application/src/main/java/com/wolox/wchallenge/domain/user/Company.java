package com.wolox.wchallenge.domain.user;

import lombok.Value;

@Value
public class Company {
    private String name;
    private String catchPhrase;
    private String bs;
}
