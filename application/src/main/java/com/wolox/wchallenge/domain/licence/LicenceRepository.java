package com.wolox.wchallenge.domain.licence;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface LicenceRepository {


    Mono<Licence> findById(Long id);
    Mono<Licence> record(Licence licence);
    Mono<Licence> change(Licence licence);
    Flux<Licence> getAllBy(LicenceType licenceType);
}
