package com.wolox.wchallenge.domain.photo;

import reactor.core.publisher.Mono;

public interface PhotoRepository {

    Mono<Photo[]> getAll();
}
