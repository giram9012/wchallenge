package com.wolox.wchallenge.domain.licence;

import com.wolox.wchallenge.domain.DomainException;

public class LicenceAlreadyExist extends DomainException {

    private static final String MESSAGE = "Already exists a licence for this album and user, update instead. %s";

    public LicenceAlreadyExist(Licence licence) {
        super(String.format(MESSAGE, licence.toString()));
    }
}