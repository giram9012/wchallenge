package com.wolox.wchallenge.domain.album;

import com.wolox.wchallenge.domain.DomainException;

public class AlbumNotFoundException extends DomainException {
    private static final String MESSAGE = "The album with id = %d not found";

    public AlbumNotFoundException(Long id) {
        super(String.format(MESSAGE, id));
    }
}
