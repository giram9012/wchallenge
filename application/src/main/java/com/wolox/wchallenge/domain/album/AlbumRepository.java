package com.wolox.wchallenge.domain.album;

import com.wolox.wchallenge.domain.photo.Photo;
import reactor.core.publisher.Mono;

public interface AlbumRepository {

    Mono<Album[]> getAll();
    Mono<Photo[]> photos(Album album);
}
