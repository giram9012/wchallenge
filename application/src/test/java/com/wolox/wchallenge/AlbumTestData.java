package com.wolox.wchallenge;

import com.wolox.wchallenge.domain.album.Album;

public class AlbumTestData {

    public static final Album[] someAlbums(){
        Album[] albums =  new Album[5];
        albums[0] = new Album(1L, 2L, "title1");
        albums[1] = new Album(2L, 2L, "title2");
        albums[2] = new Album(3L, 2L, "title3");
        albums[3] = new Album(4L, 2L, "title4");
        albums[4] = new Album(5L, 2L, "title5");
        return albums;
    }
}
