package com.wolox.wchallenge.unit.application.service;

import com.wolox.wchallenge.AlbumTestData;
import com.wolox.wchallenge.application.service.AlbumQueryService;
import com.wolox.wchallenge.domain.album.Album;
import com.wolox.wchallenge.domain.album.AlbumRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AlbumQueryServiceTest {

    @Mock
    private AlbumRepository albumRepositoryMock;

    @InjectMocks
    private AlbumQueryService albumQueryService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        albumRepositoryMock = mock(AlbumRepository.class);
        when(albumRepositoryMock.getAll()).thenReturn(Mono.just(AlbumTestData.someAlbums()));
        albumQueryService = new AlbumQueryService(albumRepositoryMock);
    }

    @AfterEach
    void tearDown() {
        albumQueryService = null;
    }

    @Test
    @DisplayName("Get all albums from the repo")
    void getAllAlbumTest() {
        //Act
        Mono<Album[]> albums = albumQueryService.getAll();

        //Assert
        assertEquals(albums.block().length, AlbumTestData.someAlbums().length);

    }

    @ParameterizedTest
    @DisplayName("find album by id")
    @ValueSource(ints = {1, 2, 3})
    void getDataRegistersTest(int id) {
        //Arrange
        Album[] albums = albumQueryService.getAll().block();
        List<Album> albumsTest = Arrays.asList(AlbumTestData.someAlbums());

        //Act
        Mono<Album> albumFound = albumQueryService.findById(albums, Long.valueOf(id));

        //Assert
        assertTrue(albumsTest.contains(albumFound.block()));

    }
}
